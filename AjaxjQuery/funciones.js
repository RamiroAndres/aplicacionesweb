/*
jQuery: Es el framework del lenguaje JavaScript más utilizado, de código abierto y software libre.
Simplifica la interacción que posee JavaScript con HTML y los eventos, además añade la interacción con AJAX en las páginas web.
La característica principal de la biblioteca es que permite cambiar el contenido de una página web sin necesidad de recargarla, mediante la manipulación del árbol DOM y peticiones AJAX. Para ello utiliza las funciones $() o jQuery().
Mejorar el rendimiento de los sites consiguiendo realizar las mismas funciones que el JavaScript con muchas menos líneas de código y en menos tiempo. Además, una de sus características principales es que permite la modificación del contenido de una web sin tener que recargar la página mediante la posibilidad de manipular el DOM y la utilización de peticiones AJAX.
*/

//Verifica que la pagina este totalmente cargada
$(document).ready(function(){
	//Revisa si se presiona el mouse
	$(".ocultaruno").click(function(){
		//oculta la clase
		$(this).hide();
	});
	//Se activa cuando se presionan dos clicks
	$(".ocultardos").dblclick(function(){
		//oculta la clase
		$(this).hide();
	});
	//Se activa la funcion cuando el mouse entre en el campo
	$(".movmouse").mouseenter(function(){
		//Muestra una alerta
		alert("Esta el cursos sobre el texto");
	});
	//Se activa cuando el mouse sale del campo
	$(".leavemouse").mouseleave(function(){
		//muestra la alerta
		alert("El cursos ha salido del texto");
	});
	//Detecta un click sobre el objeto
	$(".presionar").mousedown(function(){
		//Muestra una alerta
		alert("Presionaste el texto");
	});
	//Se activa cuando el cursor esta sobre el campo
	$(".suspender").hover(function(){
		//muestra una alerta
		alert("Colocaste el cursor sobre este parrafo");
	},
	function(){
		//cuando quita el cursor muestra una alerta
		alert("Quitaste el cursor");
	});
	$(".botonfade").click(function(){
		//cuando se hace click sobre el boton las tres caja aparecen mediante la funcion fade
		$("#caja2").fadeIn(5000);
		$("#caja3").fadeIn("fast");
		$("#caja4").fadeIn(2000);
	});
	$(".botonfade2").click(function(){
		//cuando se hace click sobre el boton las tres caja desaparecen mediante la funcion fade
		$("#caja2").fadeOut("slow");
		$("#caja3").fadeOut();
		$("#caja4").fadeOut(3000);
	});
	$(".mover").click(function(){
		//realiza una animacion sobre el objeto cuando se presiona el boton, en esta caso cambia la posicion
		$("#caja5").animate({left:'100px',top:'320px'});
	});
	$(".cambio").click(function(){
		//cuando presiona el boton la caja es animada cambiando su posicion, opacidad y tamaño
		$("#caja6").animate({
			left:'100px',
			opacity:'0.2',
			height:'50px',
			width:'50px',
		});
	});
	$(".cambio2").click(function(){
		//cambia el tamaño cada vez que da un click sobre el boton
		$("#caja7").animate({
			height:'+=50px',
			width:'+=50px'
		});
	});
	$(".cambio3").click(function(){
		//cada vez que presiona el boton la caja se esconde o aparece
		$("#caja8").animate({
			height:'toggle'
		});
	});
	$(".cambio4").click(function(){
		//guarda el objeto en una variable y luego se le aplican diferentes funciones
		var a=$("#caja9");
		//cambia el tamaño, la opacidad, la velocidad con que lo hace y el tamaño del texto
		a.animate({height:'50px',opacity:'0.2'},"slow");
		a.animate({width:'50px',opacity:'0.8'},"fast");
		a.animate({height:'75px',opacity:'0.2'},"slow");
		a.animate({width:'75px',opacity:'0.8'},"fast");
		a.animate({fontSize: '4em'},"slow");
	});
	$(".mostrar1").click(function(){
		//muestra una alerta del texto que esta oculto cuando presiona el boton
		alert("Texto: " + $("#texto").text());
	});
	$(".mostrar2").click(function(){
		//muestra una alerta del codigo html que esta oculto cuando presiona el boton
		alert("Codigo HTML: " + $("#texto").html());
	});
	$(".mostrar3").click(function(){
		//muestra una alerta con el texto que se digita en el input
		alert("Usted tiene escrito: " + $("#texto2").val());
	});
});






