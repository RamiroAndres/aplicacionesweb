/*
La primera vez que aparecio fue el 18 de Febrero de 2005 en el articulo Ajax: A New Approach to Web Applications de Jesse James Garrett
Es  un acrónimo de Asynchronous JavaScript + XML, que se puede traducir como JavaScript asíncrono + XML.

Las tecnologías que forman AJAX son:

XHTML y CSS se usan para crear una presentación basada en estándares.
DOM es utilizado para la interacción y manipulación dinámica de la presentación.
XML, XSLT y JSON son para el intercambio y la manipulación de información.
XMLHttpRequest es para intercambiar de forma asincrona informacion.
JavaScript le permite unir todas las demás tecnologías.

Las aplicaciones construidas con AJAX eliminan la recarga constante de páginas mediante la creación
de un elemento intermedio entre el usuario y el servidor.
La nueva capa intermedia de AJAX mejora la respuesta de la aplicación, ya que el usuario nunca se encuentra con una ventana del navegador vacía esperando la respuesta del servidor.
*/


$(document).ready(main);
function cargarArchivo(url){
	$("#capa").text("Espera...");
	$("#capa").load(url);
	$("#capa").header('Access-Control-Allow-Origin: *')
}
function main(){
	$("#boton1").click(function() {
		cargarArchivo("./text1.html");
	});
	$("#boton2").on("click",function() {
		cargarArchivo("./text2.html");
	});
	$("#boton3").on("click",function() {
		cargarArchivo("./text3.html");
	});
}